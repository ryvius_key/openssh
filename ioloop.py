__author__ = 'JackSong'
import socket
import sys
import errno
from tornado.ioloop import IOLoop
from tornado.gen import coroutine
from utils.multithreading import executor
from tornado.stack_context import wrap
if sys.version_info.major == 3:
    import asyncio
    import tornado.platform.asyncio

_ERRNO_WOULDBLOCK = (errno.EWOULDBLOCK, errno.EAGAIN)


class IOloop(object):
    def __init__(self, ioloop, queues):
        self.ioloop = ioloop
        self.bridges = {}
        self.queues = queues

    @staticmethod
    def instance(queues):
        if not hasattr(IOloop, "_instance"):
            IOloop._instance = IOloop(IOLoop.current(), queues)
        return IOloop._instance

    @staticmethod
    def current():
        return IOloop._instance

    def register(self, bridge):
        self.bridges[bridge.fd] = bridge
        self.add_handler(bridge.fd)

    def add_handler(self, fd):
        self.ioloop.add_handler(fd, self.read_data_callback, self.ioloop.READ | self.ioloop.ERROR)

    def update_handler(self, fd, events):
        self.ioloop.update_handler(fd, events)

    def remove_handler(self, fd):
        self.ioloop.remove_handler(fd)
        bridge = self.bridges.pop(fd, None)
        if bridge: del bridge

    @coroutine
    def read_data_callback(self, fd, event):
        bridge = self.bridges.get(fd, None)
        if event & IOLoop.READ:
            if bridge:
                data = ''
                while True:
                    try:
                        recv = yield executor(bridge.shell.recv, *(1024,))
                        if recv == None:
                            break
                        if type(recv) == bytes:
                            message = recv.decode('utf-8')
                        else:
                            message = recv
                        if len(recv) == 0:
                            break
                        data = "".join((data, message,))
                    except socket.error as e:
                        if e.errno == errno.EAGAIN:
                            self.update_handler(fd, IOLoop.WRITE)
                        elif isinstance(e, socket.timeout):
                            break
                        else:
                            self.remove_handler(fd)
                if data:
                    rest = bridge.trans_back(data)
                    yield self.queues.put(bridge.write_file(data))
                    if not rest:
                        self.remove_handler(fd)
            else:
                return

        if event & IOLoop.ERROR:
            bridge.destroy()
            self.remove_handler(fd)
