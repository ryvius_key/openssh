__author__ = 'JackSong'

import json
from sqlalchemy import create_engine
from tornado.options import options


class BaseData(object):
    def __init__(self, data=""):
        self.from_json(data)

    def from_json(self, data=""):
        self.__dict__ = json.loads(data)

    def to_json(self):
        return json.dumps(self)

    def get_type(self):
        return self.tp

    def __getattr__(self, item):
        if item in self.data.keys():
            return self.data.get(item)
        else:
            raise AttributeError()


class ClientData(BaseData):
    def __init__(self, data=""):
        super(ClientData, self).__init__(data)


class ServerData(BaseData):
    def __init__(self, data=""):
        self.tp = 'server'
        self.data = data


def get_engine():
    return create_engine(''.join(("mysql+pymysql://", options.mysql)),  encoding='utf-8', echo=False,
                   pool_size=100, pool_recycle=10)
