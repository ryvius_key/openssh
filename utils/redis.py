import redis
from tornado.options import options


def get_redis_pool():
    pool = redis.ConnectionPool(max_connections=100, host=options.redis_host,
                                port=options.redis_port,db=options.redis_db)
    return pool
