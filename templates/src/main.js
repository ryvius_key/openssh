// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import VeeValidate from 'vee-validate';
import App from './App'
import router from './router'
import store from './store'
import Navigation from './components/Navigation'

Vue.config.productionTip = false;
Vue.use(VeeValidate, {
  locale: "zh",
});
Vue.component("navigation",Navigation)
/* eslint-disable no-new */
const v=new Vue({
  el: '#app',
  store,
  router,
  components: {App},
  template: '<App/>'
});
