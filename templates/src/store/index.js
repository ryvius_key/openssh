/**
 * Created by linhaifeng on 2016/10/28.
 */
import Vue from 'vue'
import Vuex from 'vuex'
import hosts from '../store/host'
import users from '../store/user'

Vue.use(Vuex);
const store = new Vuex.Store({
  modules: {
    hosts: hosts,
    users:users
  }
});
export default store
