//设置cookie
export function setSession(value) {
  sessionStorage.setItem("session-user",value);
};

//获取cookie
export function getCookie() {
  return sessionStorage.getItem("session-user");
};

export function delCookie() {
  return sessionStorage.removeItem("session-user")
}

