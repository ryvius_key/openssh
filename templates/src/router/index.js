import Vue from 'vue'
import Router from 'vue-router'
import Host from '@/components/Host'
import Term from '@/components/Term'
import Login from '@/components/Login'
import User from '@/components/User'
import MyHost from '@/components/MyHost'
Vue.use(Router);

const router = new Router({
  routes: [
    {
      path: '/',
      name: 'MyHost',
      component: MyHost,
      meta: {requireAuth: true}
    },
    {
      path: '/hosts',
      name: 'hosts',
      component: Host,
      meta: {requireAuth: true}
    },
    {
      path: '/term/:id',
      name: 'term',
      component: Term,
      meta: {requireAuth: true}
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/user',
      name: 'user',
      component: User
    },
    {
      path: '/myHost',
      name: 'myHost',
      component: MyHost
    }
  ]
});
export default router
