import axiosService from '../utils/http'
import {fetch,del,post} from '../utils/http'

export default {
  login:function(params,func){
    axiosService.post("/login",params).then(function (res) {
        func(res)
    })
  },
  logout:function(cd){
    del("/login").then(res=>{
      cd(res)
    })
  },
  getUserIndex: function (fun) {
    fetch('/user').then(function (res) {
      fun(res)
    })
  },
  postUser: function (data,callback) {
      post('/user', data, {headers: {'Content-Type': 'multipart/form-data'}}).then(res => {
      if (res.status === false) {
        alert(res.response)
      }else{
        callback(res)
      }
    })
  },
  deleteUser: function (id) {
    del('/user', id).then(res => {
      if (res.status == false) {
        alert(res.response)
      }
    })
  },
  getUserAndHost:function(callback){
    fetch('/userAndHost').then(res=>{
      if (res.status == false) {
        alert(res.data)
      }else {
        callback(res)
      }
    })
  },
  postUserAndHost:function (ids_str,callback) {
    post('userAndHost',ids_str).then(res=>{
      if (res.status == false) {
        alert(res.data)
      }else {
        callback(res)
      }
    })
  }
}
