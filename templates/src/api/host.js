import {fetch, post, del} from '../utils/http'
import axios from 'axios';

export default {
  getIndexHosts: function (cd) {
    fetch('/host').then(function (res) {
      cd(res)
    })
  },
  postHost: function (data, callback) {
    post('/host', data).then(res => {
      if (res.status == false) {
        alert(res.data)
      } else {
        callback(res)
      }
    })
  },
  delete: function (id) {
    del('/host', id).then(res => {
      if (res.status == false) {
        alert(res.response)
      }
    })
  },
  uploadFileApi: function (file, cd) {
    let form = new FormData();
    form.append('file', file);
    axios.post('/api/upload', form, {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    }).then(res => {
      cd(res)
    })
  }

}
