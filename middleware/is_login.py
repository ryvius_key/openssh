# encoding:utf-8
import dill
from middleware.base import *


class IsLogin(MiddleWare):
    def check_login(self,handler):
        get_auth = handler.request.headers.get("Authorization")
        if not get_auth:
            return False
        user = handler.redis.get(get_auth)
        if not user:
            return False
        handler.user = dill.loads(user)
        return True


    def process_request(self,handler):
        is_login = self.check_login(handler)
        msg = "" if is_login else "not login"
        return is_login,msg
