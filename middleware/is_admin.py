from middleware.base import *
import dill


class CheckAdmin(MiddleWare):
    def check_admin(self,handler):
        get_auth = handler.request.headers.get("Authorization")
        redis_user = handler.redis.get(get_auth)
        user = dill.loads(redis_user)
        if user:
            if not user.get("is_superuser"):
                return False
        return True


    def process_request(self,handler):
        is_admin = self.check_admin(handler)
        msg = '' if is_admin else 'not is admin'
        return is_admin,msg
