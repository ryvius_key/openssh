__author__ = 'JackSong'

from tornado.options import define


def init_config():
    define('port', default=9527, type=int, help='server listening port')
    define("debug",default=True,help="Debug Mode",type=bool)
    define("log_dir",default='log',help="log dir path",type=bool)
    define('mysql',default="root:root@localhost/openssh?charset=utf8",type=str,help='mysql url')
    define('redis_host',default="127.0.0.1",type=str,help='mysql url')
    define('redis_port',default=6379,type=int,help='mysql url')
    define('redis_db',default=0,type=int,help='mysql url')
    define('salt',default="e176369e929743b3bfd675f29e44125d",type=str,help='mysql url')
    define('file_path',default="publicKey",type=str,help='public key file')
