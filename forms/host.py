from wtforms.fields import StringField,IntegerField
from wtforms.validators import Required
from wtforms_tornado import Form

class CreateHostForm(Form):
    id = IntegerField(validators=[])
    name = StringField(validators=[Required()])
    ip = StringField(validators=[Required()])
    port = StringField(validators=[Required()])
    passwd = StringField(validators=[Required()])
    username = StringField(validators=[Required()])
    key_file = StringField()
