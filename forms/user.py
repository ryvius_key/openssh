from wtforms.fields import StringField, IntegerField,BooleanField
from wtforms.validators import Required, NumberRange
from wtforms_tornado import Form


class CreateUserForm(Form):
    id = IntegerField(validators=[])
    username = StringField(validators=[Required()])
    password = StringField(validators=[Required()])
    is_superuser = BooleanField(validators=[])


class LoginForm(Form):
    username = StringField(validators=[Required()])
    password = StringField(validators=[Required()])
