__author__ = 'JackSong'

from handlers import *
from middleware import *
url_root = r"/api"
handlers = [
    ('/', IndexHandler),
    (url_root+r"/host", HostHandler,dict(middleware=(IsLogin(),CheckAdmin(),))),
    (r"/ws", WSHandler),
    (url_root+r"/userAndHost", ManagerHost,dict(middleware=(IsLogin(),))),
    (url_root+r"/user",UserHandler,dict(middleware=(IsLogin(),CheckAdmin()))),
    (url_root+r"/login",LoginHandler),
    (url_root+r"/upload",UploadFile)
]
