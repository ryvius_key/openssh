openSSH
====================

#说明

 本程序使用tornado 编写的ssh代理程序,可以将ssh转化成websocket协议,可以集成到你想要的系统中

#使用

安装依赖以及编译前台页面

`pip install -r 项目/requirements.txt`

`cd tempalates` 

`npm install`

`npm run-script build`

另外还需要mysql 与 redis 

数据库初始命令

python main.py migrate

运行

python main.py -log_file_prefix=openssh.log -port=9527

更多配置请看项目下openssh.conf

启动后访问http://127.0.0.1:9527

#已知问题

目标机器如果不是utf-8编码,会出现中文编码问题

#效果展示
查看项目下的`效果图片.png`



2018-8月更新说明

    1.增加对tornado 5版本支持和对py3 版本支持
    2.前端使用vue2重写
    3.删除前端明文传输账号，密码,IP,端口，改服务端从数据库读取
    4.增加mysql作为数据库，默认账号密码分别是root：root
    5.利用队列将操作命令输入到log目录下日志中，日志文件名是按IP,PORT,登陆账号-以及时间操作

2020-9-18 更新说明
    
    1.增加用户管理
    2.增加测试管理
    3.增加文档模式
